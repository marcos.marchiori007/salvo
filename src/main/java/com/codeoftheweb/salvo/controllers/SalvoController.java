package com.codeoftheweb.salvo.controllers;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@RestController

@RequestMapping("/api")
public class SalvoController {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private ShipRepository shiprepository;
    @Autowired
    private SalvoRepository salvoRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private ScoreRepository scoreRepository;

    @RequestMapping("/games")
    public Map<String, Object> getAll(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        if (isGuest(authentication)) {
            dto.put("player", "Guest");
        } else {
            Player player = playerRepository.findByUserName(authentication.getName());
            dto.put("player", player.playerDTO());
        }
        dto.put("games", gameRepository.findAll()
                .stream()
                .map(game -> game.gameDTO())
                .collect(Collectors.toList()));
        return dto;
    }


    @RequestMapping("/listGames")
    public List<Long> getId() {
        return gameRepository.findAll()
                .stream()
                .map(game -> game.getId())
                .collect(Collectors.toList());
    }


    @RequestMapping("/game_view/{gamePlayerId}")
    public ResponseEntity<Map<String, Object>> findGamePlayer(@PathVariable Long gamePlayerId, Authentication authentication) {
        if (isGuest(authentication)) {
            return new ResponseEntity<Map<String, Object>>(makeMap("error", "Missing data"), HttpStatus.UNAUTHORIZED);
        }
        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
        Player player = playerRepository.findByUserName(authentication.getName());

        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "Name already in use"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Name already in use"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getPlayer().getId() != player.getId()) {
            return new ResponseEntity<>(makeMap("error", "Name already in use"), HttpStatus.UNAUTHORIZED);
        }
        Map<String, Object> dto = new LinkedHashMap<>();
        Map<String,Object> hits= new HashMap<>();
        hits.put("self",new ArrayList<>());
        hits.put("opponent",new ArrayList<>());
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gameState","PLACESHIPS");
        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayers()
                .stream()
                .map(gamePlayer2 -> gamePlayer2.gamePlayerDTO())
                .collect(Collectors.toList()));
        dto.put("ships", gamePlayer.getShips()
                .stream()
                .map(ship -> ship.shipDTO())
                .collect(Collectors.toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayers()
                .stream()
                .map(gamePlayer1 -> gamePlayer1.getSalvo())
                .flatMap(salvos -> salvos.stream()).map(salvo -> salvo.salvoDTO())
                .collect(Collectors.toList()));
        dto.put("hits",hits);
         return new ResponseEntity<>(dto, HttpStatus.ACCEPTED);
    }





    @RequestMapping(path="/games/players/{gamePlayerId}/ships", method=RequestMethod.POST)
    public ResponseEntity <Map<String,Object>> createShips(@PathVariable long gamePlayerId, @RequestBody List<Ship> ships, Authentication authentication) {
        if (isGuest(authentication))
            return new ResponseEntity<>(makeMap("error", "Usted no esta logueado."), HttpStatus.UNAUTHORIZED);

        Player player = playerRepository.findByUserName(authentication.getName());

        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();

        if(gamePlayer == null)
            return new ResponseEntity<>(makeMap("error","no hay player."),HttpStatus.UNAUTHORIZED);

        if(gamePlayer.getPlayer().getId() != player.getId())
            return new ResponseEntity<>(makeMap("error","error gameplayer."),HttpStatus.UNAUTHORIZED);

        if(gamePlayer.getShips().size()>0)
            return new ResponseEntity<>(makeMap("error","Already has ships placed")
                    ,HttpStatus.FORBIDDEN);

        ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
        ships.forEach(ship -> shiprepository.save(ship));

        return new ResponseEntity<>(makeMap("OK","Let´s go!"),HttpStatus.CREATED);
    }

    @RequestMapping(value="/games/players/{gamePlayerId}/salvoes", method=RequestMethod.POST)
    public ResponseEntity <Map<String,Object>> createSalvos(@PathVariable long gamePlayerId, @RequestBody Salvo salvos, Authentication authentication) {

        if (isGuest(authentication))
            return new ResponseEntity<>(makeMap("error", "Usted no esta logueado."), HttpStatus.UNAUTHORIZED);

        Player player = playerRepository.findByUserName(authentication.getName());

        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();

        if(gamePlayer==null)
            return new ResponseEntity<>(makeMap("error","player no logueado"),HttpStatus.UNAUTHORIZED);


        if(gamePlayer.getPlayer().getId()!=player.getId())
            return new ResponseEntity<>(makeMap("error","Problemas con el gamePlayer")
                    ,HttpStatus.UNAUTHORIZED);


        if(salvos.getTurnNumber() - salvos.getLocations().size() != 1)
            return new ResponseEntity<>(makeMap("error","Already has salvoes done")
                    ,HttpStatus.FORBIDDEN);

        salvos.setGamePlayer(gamePlayer);
        salvoRepository.save(salvos);

        return new ResponseEntity<>(makeMap("OK","you can shoot")
                ,HttpStatus.CREATED);
    }

    // POST MAPPING
    @PostMapping("/games")
    public ResponseEntity<Map<String,Object>> registerGame(Authentication authentication) {
        if(isGuest(authentication))
            return new ResponseEntity<>(makeMap("error","Usted no esta logueado."),HttpStatus.UNAUTHORIZED);

        Player player= playerRepository.findByUserName(authentication.getName());

        if(player==null)
            return new ResponseEntity<>(makeMap("error","Usted no esta logueado, INAUTORIZADO PARA CONTINUAR."),HttpStatus.UNAUTHORIZED);

        Game game= gameRepository.save(new Game(LocalDateTime.now()));
        GamePlayer gamePlayer= gamePlayerRepository.save(new GamePlayer(game,player,LocalDateTime.now()));
         return new ResponseEntity<>(makeMap("gpid",gamePlayer.getId()),HttpStatus.CREATED);
    }

    @PostMapping("/game/{game_id}/players")
    public ResponseEntity<Map<String,Object>> registerJoinGame(@PathVariable Long game_id,Authentication authentication) {
        if (isGuest(authentication))
            return new ResponseEntity<>(makeMap("error", "Usted no esta logueado."), HttpStatus.UNAUTHORIZED);

        Player player = playerRepository.findByUserName(authentication.getName());

        Game game = gameRepository.findById(game_id).get();

        if(game==null)
            return new ResponseEntity<>(makeMap("error", "no such game"),HttpStatus.FORBIDDEN);

        //recorro Game para ver si esta completo (size) y le digo que compare si es mayor o igual a 2, retorna un full
        if (game.getGamePlayers().size()>=2){
            return new ResponseEntity<>(makeMap("error", "Game full"),HttpStatus.FORBIDDEN);
        }

        GamePlayer gamePlayer= gamePlayerRepository.save(new GamePlayer(game,player,LocalDateTime.now()));
        return new ResponseEntity<>(makeMap("gpid",gamePlayer.getId()),HttpStatus.CREATED);

    }
    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

}

